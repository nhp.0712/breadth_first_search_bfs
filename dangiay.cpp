#include<iostream>
#include<vector>
#include<algorithm>
#include<fstream>
#include<string>
using namespace std;

int main() {
	fstream inFile;
	inFile.open("dangiay.txt");
	int test;
	inFile >> test;
	while (test != 0) {
		int col = 0;
		int row = 0;
		inFile >> row >> col;

		vector<string> v(row);
		vector<int> pieces;
		for (int i = 0; i < row; i++)
			inFile >> v[i];

		for (int j = 0; j < col; j++) {
			int count = 0;
			for (int i = 0; i < row; i++) {
				if (v[i][j] == '1') {
					if (count != 0) {
						pieces.push_back(count);
						count = 0;
					}
				}
				else
					count++;
				
				if (i == row - 1  && count != 0)
					pieces.push_back(count);
			}
		}
		cout << pieces.size() << endl;

		pieces.clear();
		v.clear();
		test--;
	}
	
	return 0;
}