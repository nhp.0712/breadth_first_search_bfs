#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
#include<fstream>

using  namespace std;



bool flag[111][111];
bool check[111];
string input[111];
int des, paths, routes, cost;
int cor[111];
string r1, r2;
int level[111];
int ma = 0;
vector<int> vMoney;

void getInput(fstream& inFile) {
	inFile >> des >> paths >> routes;
	string a, b;
	for (int i = 0; i < des; i++)
		inFile >> input[i];

	for (int i = 0; i < des; i++)
		for (int j = 0; j < des; j++)
			flag[i][j] = 0;
	for (int i = 0; i < paths; i++) {
		inFile >> a >> b;
		for (int n = 0; n < des; n++) {
			if (input[n] == a) {
				for (int m = 0; m < des; m++) {
					if (input[m] == b) {
						flag[n][m] = 1;
						flag[m][n] = 1;
					}
				}
			}
		}
	}
}

int shipping(string r1, string r2) {
	cost = 0;
	int des1 = 0;
	int des2 = 0;
	ma = 0;
	for (int i = 0; i < des; i++) {
		if (input[i] == r1)
			des1 = i;
		if (input[i] == r2)
			des2 = i;
	}
	if (flag[des1][des2] == 1)
		ma = 1;
	else {
		int first = -1;
		int last = 0;
		cor[0] = des1;
		check[des1] = 1;
		level[0] = 0;

		while (first < last) {
			first++;
			int cur = cor[first];
			for (int next = 0; next < des; next++) {
				if (cor[last]==des2)
					continue;
				if (check[next] == 0 && flag[cur][next] == 1) {
					check[next] = 1;
					last++;
					cor[last] = next;
					level[last] = level[first] + 1;
				}
			}
		}
		if (cor[last] != des2)
			ma = 0;
		else
			ma = max(ma, level[last]);
	}
	return ma;
}

void solution(fstream& inFile) {
	vMoney.clear();

	for (int i = 0; i < routes; i++) {
		cost = 0;
		inFile >> cost >> r1 >> r2;
		for (int i = 0; i < des; i++)
				check[i] = 0;
		int money = 100 * cost * shipping(r1, r2);
		vMoney.push_back(money);
	}
}

void display() {
	for (int i = 0; i < vMoney.size(); i++) {
		if (vMoney[i] != 0)
			cout << "$" << vMoney[i] << endl;
		else
			cout << "NO SHIPMENT POSSIBLE" << endl;
	}
	cout << endl;
}

int main() {
	fstream inFile;
	inFile.open("shipping.txt");
	int test;
	inFile >> test;
	cout << "SHIPPING ROUTES OUTPUT\n\n";
	for (int i = 1; i <= test; i++) {
		cout << "DATA SET " << i << endl << endl;
		getInput(inFile);
		solution(inFile);
		display();
	}
	return 0;
}