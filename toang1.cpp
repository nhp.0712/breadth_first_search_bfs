#include<iostream>
#include<vector>
#include<algorithm>
#include<fstream>
using namespace std;


struct coordinate {
	int r, c;
};

int row, col;
int input[111][111];
bool check[111][111];
coordinate cor[11111];
vector<int> v;

coordinate direction[4] = { -1,0,
						 0,1,
						 1,0,
						 0,-1 };

coordinate move(coordinate from, int dir) {
	coordinate to;
	to.c = from.c + direction[dir].c;
	to.r = from.r + direction[dir].r;
	return to;
}

bool isOut(coordinate cor) {
	if (cor.c < 0 || cor.c >= col)
		return 1;
	if (cor.r < 0 || cor.r >= row)
		return 1;
	return 0;
}

void getInput(fstream& inFile) {
	inFile >> row >> col;

	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			inFile >> input[i][j];
			check[i][j] = 0;
		}
	}
}

int toang1(int r, int c) {
	int first = -1;
	int last = 0;

	coordinate cur, next;
	cur.c = c;
	cur.r = r;
	cor[0] = cur;
	check[r][c] = 1;
	
	while (first < last) {
		first++;
		cur = cor[first];

		for (int i = 0; i < 4; i++) {
			next = move(cur, i);

			if (isOut(next))
				continue;

			if (check[next.r][next.c] == 0 && input[next.r][next.c] == 0) {
				check[next.r][next.c] = 1;
				last++;
				cor[last] = next;
			}
		}
	}

	return last + 1;
}

void solution() {
	v.clear();
	for (int i = 0; i < row; i++) {
		for (int j = 0;  j < col; j++) {
			if (check[i][j] == 0 && input[i][j] == 0) {
				int area = toang1(i, j);
				v.push_back(area);
			}
		}
	}
	sort(v.begin(), v.end());
}

void display() {
	cout << v.size() << " ";
	for (int i = 0; i < v.size(); i++)
		cout << v[i] << " ";
	cout << endl;
}
int main() {
	fstream inFile;
	inFile.open("toang1.txt");
	int test;
	inFile >> test;
	for (int i = 0; i < test; i++) {
		getInput(inFile);
		solution();
		display();
	}
	return 0;
}