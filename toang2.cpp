#include<iostream>
#include<fstream>
#include<algorithm>
#include<vector>
#include<string>
#include<cmath>
using namespace std;

int villages, paths;
bool flag[111][111];
int input[111];
bool check[111];

void getInput(fstream& inFile) {
	int vil1, vil2;
	inFile >> villages >> paths;
	for (int i = 1; i <= villages; i++)
		for (int j = 1; j <= villages; j++)
			flag[i][j] = 0;
	for (int i = 0; i < paths; i++) {
		inFile >> vil1 >> vil2;
		flag[vil1][vil2] = 1;
		flag[vil2][vil1] = 1;
	}
}

void toang(int vil) {
	int first = -1;
	int last = 0;

	input[0] = vil;

	check[vil] = 1;

	while (first < last) {
		first++;
		int current = input[first];

		for (int next = 1; next <= villages; next++) {
			if (next != current && check[next] == 0 && flag[current][next] == 1) {
				check[next] = 1;
				last++;
				input[last] = next;
			}
		}
	}
}

void solution() {
	for (int i = 1; i <= villages; i++)
		check[i] = 0;
	int countProvince = 0;
	for (int i = 1; i <= villages; i++) {
		if (check[i] == 0) {
			toang(i);
			countProvince++;
		}
	}
	cout << countProvince << endl;
}

int main() {
	fstream inFile;
	inFile.open("toang2.txt");
	int test = 0;
	inFile >> test;
	while (test != 0) {
		getInput(inFile);
		solution();
		test--;
	}
	system("pause");
	return 0;
}