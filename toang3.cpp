#include<iostream>
#include<vector>
#include<algorithm>
#include<fstream>
using namespace std;

vector<int> hold,v,getp;
vector<vector<int>> p;
int villages, paths;
int countVil = 1;
int input[111];
bool flag[111][111];
bool check[111];
int level[111];
int ma = 0;
void getInput(fstream &inFile) {
	int a, b = 0;
	inFile >> villages >> paths;
	for (int i = 1; i <= villages; i++)
		for (int j = 1; j <= villages; j++)
			flag[i][j] = 0;

	for (int i = 0; i < paths; i++) {
		inFile >> a >> b;
		flag[a][b] = 1;
		flag[b][a] = 1;
	}
}

void toang3(int vil) {
	int first = -1;
	int last = 0;

	input[0] = vil;
	check[vil] = 1;
	level[1] = 0;
	while (first < last) {
		first++;
		int current = input[first];

		for (int next = 1; next <= villages; next++) {
			if (next != current && check[next] == 0 && flag[current][next] == 1) {
				check[next] = 1;
				last++;
				input[last] = next;
				countVil++;
				level[last] = level[first] + 1;
			}	
		}
	}
	ma = max(level[last],ma);
}

void solution() {
	for (int i = 1; i <= villages; i++) {
		//level[i] = 1;
		check[i] = 0;
	}
	int countProvince = 0;
	
	for (int i = 1; i <= villages; i++) {
		if (check[i] == 0) {
			toang3(i);
			countProvince++;
			hold.push_back(countVil);
			countVil = 1;
		}
	}

	cout << countProvince << " ";

	sort(hold.begin(), hold.end());
	for (int i = hold.size() - 1; i >= 0; i--)
		cout << hold[i] << " ";
		cout << ma << " ";
	cout << endl;
}
int main() {
	fstream inFile;
	int test = 0;
	inFile.open("toang3.txt");
	inFile >> test;

	while (test != 0) {
		hold.clear();
		getInput(inFile);
		solution();
		test--;
	}
	return 0;
}