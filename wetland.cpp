#include<iostream>
#include<fstream>
#include<algorithm>
#include<vector>
#include<string>

using namespace std;

struct coordinate {
	int r, c;
};

coordinate direction[9] = { -1,0,
						-1,1,
						0,1,
						1,1,
						1,0,
						1,-1,
						0,-1,
						-1,-1
};
int row, col, line;
vector<int> v, vDis, vTree;
char input[111][111];
bool check[111][111];
coordinate cor[11111];
int level[111];
int ma = 0;
int tree = 0;
void clearData() {
	for (int i = 0; i < 111; i++) {
		for (int j = 0; j < 111; j++) {
			check[i][j] = 0;
			input[i][j] = '\0';
		}
	}
}

bool isOut(coordinate cor) {
	if (cor.c < 1 || cor.c > col)
		return 1;
	if (cor.r < 1 || cor.r > row)
		return 1;
	return 0;
}
void getInput(fstream& inFile) {
	clearData();
	inFile >> row >> col >> line;
	for (int i = 1; i <= row; i++)
		for (int j = 1; j <= col; j++)
			inFile >> input[i][j];

}

coordinate move(coordinate from, int dir) {
	coordinate to;

	to.c = from.c + direction[dir].c;
	to.r = from.r + direction[dir].r;

	return to;
}
int wetland(coordinate inputCor) {
	int first = -1;
	int last = 0;
	coordinate cur, next;
	cur.c = inputCor.c;
	cur.r = inputCor.r;
	cor[0] = cur;
	check[inputCor.r][inputCor.c] = 1;
	level[0] = 0;
	ma = 0;
	tree = 0;
	while (first < last) {
		first++;
		cur = cor[first];

		for (int i = 0; i < 8; i++) {
			next = move(cur, i);

			if (check[next.r][next.c] == 0 && input[next.r][next.c] == 'W') {
				if (isOut(next))
					continue;
				check[next.r][next.c] = 1;
				last++;
				cor[last] = next;
				level[last] = level[first] + 1;
			}
			if ((check[next.r][next.c] == 0 && input[next.r][next.c] == 'W') || (check[next.r][next.c] == 0 && input[next.r][next.c] == 'L')) {
				if (isOut(next))
					continue;
				check[next.r][next.c] = 1;
				tree++;
			}
		}
	}
	ma = max(ma, level[last]);
	return last + 1;
}

void solution(fstream& inFile) {
	v.clear();
	vDis.clear();
	vTree.clear();
	coordinate inputCor;
	int r, c;
	for (int i = 0; i < line; i++) {
		inFile >> r >> c;
		inputCor.r = r;
		inputCor.c = c;
		for (int i = 0; i < row; i++)
			for (int j = 0; j < col; j++)
				check[i][j] = 0;

		int area = wetland(inputCor);
		v.push_back(area);
		vDis.push_back(ma);
		vTree.push_back(tree);
	}

}

void display() {
	for (int i = 0; i < v.size(); i++)
		cout << v[i] << " " << vDis[i] << endl;
	for (int i = 0; i < vTree.size(); i++)
		if (tree <= vTree[i])
			tree = vTree[i];
	cout << tree << endl;
	cout << endl;

}

int main() {
	fstream inFile;
	inFile.open("wetland.txt");
	int test;
	inFile >> test;
	while (test != 0) {
		getInput(inFile);
		solution(inFile);
		display();
		test--;
	}
	return 0;
}
