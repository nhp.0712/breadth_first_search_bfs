#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
#include<fstream>
using namespace std;

vector<string> input;
vector<int> sizeFlag;
bool check[200];
int q[200];
bool flag[200][200];
int level[200];
int ma = -1;
void getInput(fstream& inFile) {
	input.clear();
	for (int i = 0; i < 200; i++)
		for (int j = 0; j < 200; j++)
			flag[i][j] = 0;

	string star = "0";
	while (star != "*") {
		inFile >> star;
		if (star != "*")
			input.push_back(star);
	}
	for (int n = 0; n < input.size(); n++) {
		string temp = input[n];
		for (int m = 0; m < input.size(); m++) {
			int count = 0;
			if (n != m && temp.size() == input[m].size()) {
				for (int i = 0; i < temp.size(); i++) {
					if (temp[i] != input[m][i])
						count++;
				}
				if (count == 1) {
					flag[n][m] = 1;
					flag[m][n] = 1;
				}
			}
		}
	}
}

int wordtran(string s1, string s2) {
	int l1 = s1.size();
	int l2 = s2.size();
	int pos = 0;
	ma = 0;
	if (l1 != l2)
		return 0;

	for (int i = 0; i < input.size(); i++) {
		if (input[i] == s1) {
			pos = i;
			break;
		}
	}
	int first = -1; 
	int last = 0;
	q[0] = pos;
	check[pos] = 1;
	level[0] = 0;
	while (first < last) {
		first++;
		int cur = q[first];
		for (int next = 0; next < input.size(); next++) {	
			if (input[q[last]] != s2 && check[next] == 0 && flag[cur][next] == 1) {
				int count = 0;
				for (int i = 0; i < input[cur].size(); i++)
					if (input[cur][i] != input[next][i])
						count++;
				if (count == 1) {
					check[next] = 1;
					last++;
					q[last] = next;
					level[last] = level[first] + 1;
				}
			}
		}
	}
	if (input[q[last]] != s2)
		ma = 0;
	else
		ma = max(ma, level[last]);
	return ma;
}

void solution(fstream& inFile) {
	while (!inFile.eof()) {
		string s1, s2;
		inFile >> s1 >> s2;
		for (int i = 0; i < 200; i++)
			check[i] = 0;
		ma = 0;
		int step = wordtran(s1, s2);
		cout << s1 << " " << s2 << " " << step << endl;
	}
}

int main() {
	fstream inFile;
	inFile.open("wordtran.txt");
	int test = 0;
	inFile >> test;
	while (test != 0) {
		getInput(inFile);
		solution(inFile);
		test--;
		cout << endl;
	}
	return 0;
}